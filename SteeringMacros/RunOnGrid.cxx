#include <EventLoop/DirectDriver.h>
#include <EventLoop/Job.h>
#include <MyAnalysis/MyxAODAnalysis.h>
#include <TSystem.h>
#include <SampleHandler/ScanDir.h>

#include <SampleHandler/ToolsDiscovery.h>

  //===========================================                                           
  // On command line do:                                                                  
  // root -l '$ROOTCOREDIR/scripts/load_packages.C' '../source/EP_nTupleMaker/SteeringMacros/RunOnGrid.cxx ("OutputDirectory")'
  //==========================================          


void RunOnGrid (const std::string& submitDir)
{
  // Set up the job for xAOD access:
  xAOD::Init().ignore();

  // create a new sample handler to describe the data files we use
  SH::SampleHandler sh;

  // scan for datasets in the given directory
  // this works if you are on lxplus, otherwise you'd want to copy over files
  // to your local machine and use a local path.  if you do so, make sure
  // that you copy all subdirectories and point this to the directory
  // containing all the files, not the subdirectories.

  // use SampleHandler to scan all of the subdirectories of a directory for particular MC single file:
  //const char* inputFilePath = gSystem->ExpandPathName ("$ALRB_TutorialData/r9315/");
 // SH::ScanDir().filePattern("AOD.11182705._000001.pool.root.1").scan(sh,inputFilePath);
      
//const char* inputFilePath = gSystem->ExpandPathName ("/afs/cern.ch/user/c/chweber/myWorkspace/data/eGamma/mc16a_13TeV_singleElectron_noPileup");
//SH::ScanDir().filePattern("AOD.11249837._000001.pool.root.1").scan(sh,inputFilePath);

  
  //SH::scanRucio (sh, "mc16_13TeV.423000.ParticleGun_single_electron_egammaET.deriv.DAOD_EGAM1.e3566_s3113_r9388_r9315_p3282");  //MC16 single electron sample eGamma derivation
  //SH::scanRucio (sh, "mc16_13TeV.423000.ParticleGun_single_electron_egammaET.merge.AOD.e3566_s3113_r9388_r9315"); //MC16 single electron sample
  SH::scanRucio (sh, "mc16_13TeV.423000.ParticleGun_single_electron_egammaET.merge.AOD.e3566_s3113_r9641_r9315"); //MC16a electron pileup sample

  // set the name of the tree in our files
  // in the xAOD the TTree containing the EDM containers is "CollectionTree"
  sh.setMetaString ("nc_tree", "CollectionTree");

  // further sample handler configuration may go here

  // print out the samples we found
  sh.print ();

  // this is the basic description of our job
  EL::Job job;
  
  //// maybe only use one of these 2 options per job??
  job.options()->setDouble(EL::Job::optGridNFilesPerJob, 1 );     // set to 5 for the AOD, maybe set to 1 for the xAOD
  //job.options()->setDouble(EL::Job::optGridNJobs, 5);ger(optGridNJobs, 5);


  job.sampleHandler (sh); // use SampleHandler in this job
  //job.options()->setDouble (EL::Job::optMaxEvents, 500); // for testing purposes, limit to run over the first 500 events only!
  job.options()->setDouble (EL::Job::optMaxEvents, -1); // run over all the samples

  // define an output and an ntuple associated to that output
  EL::OutputStream output  ("myOutput");
  job.outputAdd (output);
  EL::NTupleSvc *ntuple = new EL::NTupleSvc ("myOutput");
  job.algsAdd (ntuple);

  // add our algorithm to the job
  MyxAODAnalysis *alg = new MyxAODAnalysis;

  // set the name of the algorithm (this is the name use with
  // messages)
  alg->SetName ("AnalysisAlg");

  alg->outputName = "myOutput"; // give the name of the output to our algorithm

  // later on we'll add some configuration options for our algorithm that go here

  job.algsAdd (alg);

  // make the driver we want to use:
  // this one works by running the algorithm directly:
  //EL::DirectDriver driver;
  EL::PrunDriver driver;
  // we can use other drivers to run things on the Grid, with PROOF, etc.

  //driver.options()->setString("nc_outputSampleName", "user.chweber.mc16_13TeV.423000.ParticleGun_single_electron_egammaET.deriv.DAOD.EpCombination.nTuple.03");
  driver.options()->setString("nc_outputSampleName", "user.chweber.mc16_13TeV.423000.ParticleGun_single_electron_egammaET.deriv.xAOD.EpCombination.nTuple.pileup.01.%in:name[2]%");


  // process the job using the driver
  driver.submit (job, submitDir);
}