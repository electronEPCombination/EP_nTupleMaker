=====================================================================================================================
 Code to assemble nTuples from xAODs for calculating likelihood PDFS for the EP Combinaiton
=====================================================================================================================



--------------------------------------------------
**Compiling the code**

1) Login onto lxplus.

2) Setup ATLAS:

      setupATLAS

3) Create the directry where you wanna place the project, and subdirectories 

      mkdir EP_nTuple_project
      cd EP_nTuple_project
      mkdir source build run

4) Go up to source and clone the project

      cd source
      git clone https://gitlab.cern.ch/electronEPCombination/EP_nTupleMaker

5) Setup the Analysis release

      cd ../build
      asetup 21.2,AnalysisBase,latest,here
      mv CMakeLists.txt ../source

6) Build the project

      cmake ../source
      source $TestArea/*/setup.sh
      cmake --build $TestArea  # this can be called from any directory





You must do the following everytime you reset your environment (i.e. new shell):
```
cd EP_nTuple_project/build
asetup --restore
source $TestArea/*/setup.sh
```







---------------------------------------------------
 **RUNNING THE CODE:**

In order to run the code, after you are in the project directory, setup atlas , setup the analysis release, and compiled, do the following:

1) Go up the the "Run" directory:

     cd run

2) Execute the code. It will depend on what and where you want to run the code:

  a) Locally, and reading the data from a local directory (usually done for cut flow challenge): 

     root -l '$ROOTCOREDIR/scripts/load_packages.C' '../source/EP_nTupleMaker/SteeringMacros/ATestRun.cxx ("OutputDirectory")'

     #(You have to set the correct path and the sample in the file ATestRun.cxx)

  

 In all cases above "OutputDirectory" corresponds to the directory where all your output will be saved (minitrees, histograms...).
 You have to provide it everytime that you run the code. If there is an existing directory with that name the code
 will crash, so you have to remove the old directory, or provide a different name. 

  **RUNNING THE CODE ON THE GRID:**
  
        lsetup panda
        root -l '$ROOTCOREDIR/scripts/load_packages.C' '../source/EP_nTupleMaker/SteeringMacros/RunOnGrid.cxx ("GridOutputDirectory")'


 
    
    
____________________________________________________________________________________     


Remember:
    Push the commited changes to the repository here:
    git push -u origin master
