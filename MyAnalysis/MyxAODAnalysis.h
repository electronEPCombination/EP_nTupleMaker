#ifndef MyAnalysis_MyxAODAnalysis_H
#define MyAnalysis_MyxAODAnalysis_H

#include <EventLoop/Algorithm.h>

#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"


// Root thingies 
#include <TTree.h> // to store things in TTrees

namespace CP {
    class EgammaCalibrationAndSmearingTool;
}

class MyxAODAnalysis : public EL::Algorithm
{
  // put your configuration variables here as public variables.
  // that way they can be set directly from CINT and python.
public:
  // float cutValue;

   // an event counter, fowllowing the xAOD Analysis in EventLoop
   // https://twiki.cern.ch/twiki/bin/viewauth/AtlasComputing/SoftwareTutorialxAODAnalysisInROOT#6_Objects_and_tools_for_analysis
   int m_eventCounter =0 ; //!



  // variables that don't get filled at submission time should be
  // protected from being send from the submission node to the worker
  // node (done by the //!)
private:
 /// description: the event we are reading from
    xAOD::TEvent *event; //!  // https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/EventLoop#Access_the_Data_Through_xAOD_EDM

    bool doDebug = false;//!

   
public:
  // Tree *myTree; //!
  // TH1 *myHist; //!
// OUTPUT FILES 
  // defining the output file name and tree that we will put in the output ntuple, also the one branch that will be in that tree 
    std::string outputName;
    TTree *tree; //!

    int eventNumber; //!



    // (trueParticlePtr)->pt();
    double electronTruePt;
    // (trueParticlePtr)->eta();
    double electronTrueEta; //!

    // (electron)->eta();
    double electronEta; //!
    // (electron)->pt();
    double electronPt; //!
    // (electron)->e();
    double electronE; //!
    // (electron)->e() calibrated;
    double electronE_calibrated; //!

    // (electron)->caloCluster()->e();
    double electronCaloE; //!
    // (electron)->caloCluster()->eta();
    double electronCaloEta; //!

    // (electron)->trackParticle()->pt();
    double electronTrackerPt; //!

    double electronTrackerLastMeasuredP; //!
    double electronTrackerFirstMeasuredP; //!
    double electronTrackerPerigeeP; //!

    //double fBrem; //! 1.0-fabs(trackersLastMeasP/trackerElectronPerigeeP)

    int particleTruthType; //!

    CP::EgammaCalibrationAndSmearingTool* energy_tool; //!



  // this is a standard constructor
  MyxAODAnalysis ();

  // these are the functions inherited from Algorithm
  virtual EL::StatusCode setupJob (EL::Job& job);
  virtual EL::StatusCode fileExecute ();
  virtual EL::StatusCode histInitialize ();
  virtual EL::StatusCode changeInput (bool firstFile);
  virtual EL::StatusCode initialize ();
  virtual EL::StatusCode execute ();
  virtual EL::StatusCode postExecute ();
  virtual EL::StatusCode finalize ();
  virtual EL::StatusCode histFinalize ();

  // this is needed to distribute the algorithm to the workers
  ClassDef(MyxAODAnalysis, 1);
};

#endif