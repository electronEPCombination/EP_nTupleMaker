#include <AsgTools/MessageCheck.h>
#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>
#include <MyAnalysis/MyxAODAnalysis.h>


//This should be the container for electrons
#include "xAODEgamma/ElectronContainer.h"

// this should help me to find the 'truth electron' to a given reconstructed electron
#include "xAODTruth/xAODTruthHelpers.h"
////And this should enable me to access the true electron pT (if I have MonteCarlo electrons)
#include "xAODTruth/TruthParticle.h"

// EDM includes:
//https://twiki.cern.ch/twiki/bin/viewauth/AtlasComputing/SoftwareTutorialxAODAnalysisInROOT#6_Objects_and_tools_for_analysis
#include "xAODEventInfo/EventInfo.h"

// Root thingies
#include <TFile.h> // for writing a root files (?)

// do the electron energy calibration
#include <ElectronPhotonFourMomentumCorrection/EgammaCalibrationAndSmearingTool.h>

//to do shallow copies of xAODs
#include <xAODCore/ShallowCopy.h>


// this is needed to distribute the algorithm to the workers
ClassImp(MyxAODAnalysis)



MyxAODAnalysis :: MyxAODAnalysis ()
{
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0.  Note that you should only put
  // the most basic initialization here, since this method will be
  // called on both the submission and the worker node.  Most of your
  // initialization code will go into histInitialize() and
  // initialize().
}



EL::StatusCode MyxAODAnalysis :: setupJob (EL::Job& job)
{
  // Here you put code that sets up the job on the submission object
  // so that it is ready to work with your algorithm, e.g. you can
  // request the D3PDReader service or add output files.  Any code you
  // put here could instead also go into the submission script.  The
  // sole advantage of putting it here is that it gets automatically
  // activated/deactivated when you add/remove the algorithm from your
  // job, which may or may not be of value to you.
  
  // let's initialize the algorithm to use the xAODRootAccess package
  job.useXAOD ();

  return EL::StatusCode::SUCCESS;
}



EL::StatusCode MyxAODAnalysis :: histInitialize ()
{
  // Here you do everything that needs to be done at the very
  // beginning on each worker node, e.g. create histograms and output
  // trees.  This method gets called before any input files are
  // connected.

  // get the output file, create a new TTree and connect it to that output
    TFile *outputFile = wk()->getOutputFile (outputName);
    tree = new TTree ("tree", "tree");
    tree->SetDirectory (outputFile);
    // define what braches will go in that tree   

    tree->Branch("EventNumber", &eventNumber);

    tree->Branch("eTruePt", &electronTruePt);     // (trueParticlePtr)->pt();
    tree->Branch("eTrueEta", &electronTrueEta);   // (trueParticlePtr)->eta();

    tree->Branch("eEta", &electronEta); 
    tree->Branch("ePt", &electronPt);
    tree->Branch("eE", &electronE);
    tree->Branch("eE_calibrated", &electronE_calibrated);

    tree->Branch("eCaloE", &electronCaloE);
    tree->Branch("eCaloEta", &electronCaloEta);

    tree->Branch("eTrackPt", &electronTrackerPt);

    tree->Branch("eLastMeasP", &electronTrackerLastMeasuredP);
    tree->Branch("eFirstMeasP", &electronTrackerFirstMeasuredP);
    tree->Branch("ePerigeeP", &electronTrackerPerigeeP);

    tree->Branch("truthTypeOfParticle", &particleTruthType);


  //====================================================================================
  // Egamma calibration tool
  //====================================================================================

  energy_tool = new CP::EgammaCalibrationAndSmearingTool("energy_tool");
  ATH_MSG_INFO("initializing calibration tool");

  energy_tool->setProperty("ESModel", "es2017_R21_PRE");
/* Options of the EgammaCalibrationAndSmearingTool that I am not going to deal with now
  energy_tool->setProperty("doSmearing", use_smearing);
  energy_tool->setProperty("doScaleCorrection", use_scales);
  energy_tool->setProperty("useLayerCorrection", use_layer_corrections);
*/
energy_tool->setProperty("randomRunNumber", "123456");

  // Turn off gain correction
  energy_tool->setProperty("useGainCorrection", 0);

  //-----------------------------------------------------------------------------------------
  // !! Remove scales and smearing for computation nTuples
  //-----------------------------------------------------------------------------------------
  energy_tool->setProperty("doSmearing", 0);
  energy_tool->setProperty("doScaleCorrection", 0);
  //-----------------------------------------------------------------------------------------

  // TODO: other correction to switch?
  if (not energy_tool->initialize().isSuccess()) {
    ATH_MSG_ERROR("Failed to initialize EgammaCalibrationAndSmearingTool");
    return EL::StatusCode::FAILURE;
  }



  return EL::StatusCode::SUCCESS;
}



EL::StatusCode MyxAODAnalysis :: fileExecute ()
{
  // Here you do everything that needs to be done exactly once for every
  // single file, e.g. collect a list of all lumi-blocks processed
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode MyxAODAnalysis :: changeInput (bool /*firstFile*/)
{
  // Here you do everything you need to do when we change input files,
  // e.g. resetting branch addresses on trees.  If you are using
  // D3PDReader or a similar service this method is not needed.
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode MyxAODAnalysis :: initialize ()
{
  // Here you do everything that you need to do after the first input
  // file has been connected and before the first event is processed,
  // e.g. create additional histograms based on which variables are
  // available in the input files.  You can also create all of your
  // histograms and trees in here, but be aware that this method
  // doesn't get called if no events are processed.  So any objects
  // you create here won't be available in the output if you have no
  // input events.

  ANA_MSG_INFO ("in initialize");

  return EL::StatusCode::SUCCESS;
}



EL::StatusCode MyxAODAnalysis :: execute ()
{
  // Here you do everything that needs to be done on every single
  // events, e.g. read input variables, apply cuts, and fill
  // histograms and trees.  This is where most of your actual analysis
  // code will go.

  //eventloop has a special way of interfacing with the event store
  //so define a variable of type xAOD::TEvent to access information from xAODs
  // https://twiki.cern.ch/twiki/bin/viewauth/AtlasComputing/SoftwareTutorialxAODAnalysisInROOT#4_Accessing_xAOD_quantities
  xAOD::TEvent* event =  wk()->xaodEvent();


  // 6. Objects and tools for analysis
  //https://twiki.cern.ch/twiki/bin/viewauth/AtlasComputing/SoftwareTutorialxAODAnalysisInROOT#General_event_information
        // print every 100 events, so we know where we are:
//          if( (m_eventCounter % 100) ==0 ) Info("execute()", "Event number = %i", m_eventCounter );
//          m_eventCounter++;

          //----------------------------
          // Event information
          //--------------------------- 
          const xAOD::EventInfo* eventInfo = 0;
          //ANA_CHECK(event->retrieve( eventInfo, "EventInfo"));  
          event->retrieve( eventInfo, "EventInfo");  
          
          // check if the event is data or MC
          // (many tools are applied either to data or MC)
          bool isMC = false;
          // check if the event is MC
          if(eventInfo->eventType( xAOD::EventInfo::IS_SIMULATION ) ){ isMC = true ;} // can do something with this later


  //6. Objects and tools for analysis

  // analog to 6. Objects and tools for analysis / electrons
  //https://twiki.cern.ch/twiki/bin/viewauth/AtlasComputing/SoftwareTutorialxAODAnalysisInROOT#Muons
        //------------
        // Electrons
        //------------
          // get electron container of interest
          const xAOD::ElectronContainer* electrons = 0;
          //ANA_CHECK(event->retrieve( electrons, "Electrons" )); // I haven't bothered yet to implement ANA_CHECK
          event->retrieve( electrons, "Electrons" );

          //Make a shallow copy of the electron container
          std::pair<xAOD::ElectronContainer*, xAOD::ShallowAuxContainer*> my_electrons_container_plus_aux_check = xAOD::shallowCopyContainer(*electrons); 
          // no quite syure? Maybe pointer to the shallow copy electron container?
          xAOD::ElectronContainer* my_electrons_check = my_electrons_container_plus_aux_check.first; 

          // loop over the electrons in the container
          for(xAOD::Electron* electron : *my_electrons_check ) { //loop over electrons
              // electron -> 'method' lets you access the methods of the current electrion now

              // get the TrackParticle object associated with the electron (xAOD::TrackParticle comes already with the xAOD::ElectronContainer)
              const xAOD::TrackParticle* ptrTo_electronsTrack = (electron) -> trackParticle();    
              double trackersLastMeasP = 0;
              double trackersFirstMeasP = 0;
              double trackerElectronPerigeeP = 0;
              if (ptrTo_electronsTrack){ // do this only if ptrTo_electronsTrack is not the null pointer
                  if(doDebug){  Info("execute()", "electron Track pointer %p", ptrTo_electronsTrack); }; // print the pointer to the screen

                  unsigned int index; // use this to index which measurement of the tracker to use

                  // calculate the momentum at the last measurement
                  if( ptrTo_electronsTrack->indexOfParameterAtPosition(index, xAOD::LastMeasurement) ) {
            
                          trackersLastMeasP  = sqrt(std::pow(ptrTo_electronsTrack->parameterPX(index), 2) +
                                                    std::pow(ptrTo_electronsTrack->parameterPY(index), 2) +
                                                    std::pow(ptrTo_electronsTrack->parameterPZ(index), 2));

                          trackersFirstMeasP  = sqrt(std::pow(ptrTo_electronsTrack->parameterPX(0), 2) +
                                                      std::pow(ptrTo_electronsTrack->parameterPY(0), 2) +
                                                      std::pow(ptrTo_electronsTrack->parameterPZ(0), 2));

                          trackerElectronPerigeeP = 1/(ptrTo_electronsTrack->qOverP());
                  } // if( ptrTo_electronsTrack->indexOfParameterAtPosition(index, xAOD::LastMeasurement) )
              } // if (ptrTo_electronsTrack){


            particleTruthType = xAOD::TruthHelpers::getParticleTruthType(*electron); // get the true type of the particle
            // Meaning of the ids: Unknown = 0, // UnknownElectron = 1, // IsoElectron =  2, // NonIsoElectron = 3, // BkgElectron = 4,
            // Got these numbers from here: https://svnweb.cern.ch/trac/atlasoff/browser/PhysicsAnalysis/MCTruthClassifier/trunk/MCTruthClassifier/MCTruthClassifierDefs.h

             // initialize and define pointer to the true particle (as we are dealing with MC data right now)
             const xAOD::TruthParticle* trueParticlePtr = xAOD::TruthHelpers::getTruthParticle(*electron); // of the particleTruthType is != 2 we seem to get the null pointer more often than not.
              if(doDebug){  Info("execute()", "trueParticlePtr pointer %p", trueParticlePtr); };// print the pointer to the screen














             if(particleTruthType > -1 && trueParticlePtr){ // consider all particle types for now, otherwise I believe true IsoElectron are the 'proper' electrons
                  // (electron)->pt() // pt from tracker [MeV?]
                  // (electron)->eta() // eta from tracker
                  // (electron)->caloCluster()->pt() // pt from calorimeter [MeV?]
                  // (electron)->caloCluster()->e() // energy from calorimeter [MeV?]
                  // (electron)->caloCluster()->eta() // eta from calorimeter
                  //(trueParticlePtr)->pt() // true particle pt [MeV?]

                  //double pt_TrackVsTrueFractionalDifference = ((electron)->pt()) / ((trueParticlePtr)->pt()) - 1; // relative difference of track pt and true pt
                  //double  pt_CaloVsTrueFractionalDifference = ((electron)->caloCluster()->pt()) / ((trueParticlePtr)->pt()) - 1; // relative difference of calorimeter pt and true pt

                  // print variables to screen
                    //Info("execute()", "tracker pt = %.1f MeV, calo pt = %.1f MeV, true pt = %.1f MeV, tracker %% diff = %.3f, calo %% diff = %.3f, trackers last P = %.3f",
                    //(electron)->pt(), (electron)->caloCluster()->pt(), (trueParticlePtr)->pt(), pt_TrackVsTrueFractionalDifference, pt_CaloVsTrueFractionalDifference, trackersLastMeasP); // just to print out something, seems to use 'printf' command of c++

                  // select data to put into our tree / nTuple
                    eventNumber = eventInfo->eventNumber();
                    electronTrackerPt   = (electron)->trackParticle()->pt();
                    electronCaloE     = (electron)->caloCluster()->e();
                    electronEta    = (electron)->eta();
                    electronCaloEta = (electron)->caloCluster()->eta();
                    electronTruePt = (trueParticlePtr)->pt();
                    electronTrueEta = (trueParticlePtr)->eta();
                    electronTrackerLastMeasuredP  = trackersLastMeasP;
                    electronTrackerFirstMeasuredP = trackersFirstMeasP;
                    electronTrackerPerigeeP = trackerElectronPerigeeP;
                    //particleTruthType -- has already been filled

                    electronPt = (electron)->pt();
                    electronE  = (electron)->e();


                    energy_tool->applyCorrection(*electron);
                    electronE_calibrated = electron->e();

                    // fBrem = 1.0-fabs(trackersLastMeasP/trackerElectronPerigeeP); //calculate fBrem

                    //Do the EP combination             ( pT_track        ,  pT_cluster                          , eta_cluster    , eta_track         , Ecluster     , dPoverPin, &ptCombined, &ptError)
                    //myEPcombinationTool -> getCombinedPt(electronTrackerPt, electronCaloE / cosh(electronTrueEta), electronCaloEta, electronEta, electronCaloE, fBrem    , ptCombined , ptError);
                    // put the combined pT into our tree
                    //electronCombinedPt = ptCombined;

                    tree->Fill(); // put data into our tree / nTuple

             } // if(particleTruthType == 2 )



        //const xAOD::TruthParticle* particle = event->truthParticle(trueParticlePtr);

        //Info("execute()", "tracker pt = %.3f, true pt = %.3f",(electron)->pt(),(trueParticlePtr)->pt()); // just to print out something, seems to use 'printf' command of c++
        

  //got Electron truth variables



          } // end for loop over electrons
  // analog to 6. Objects and tools for analysis / electrons




  return EL::StatusCode::SUCCESS;
}



EL::StatusCode MyxAODAnalysis :: postExecute ()
{
  // Here you do everything that needs to be done after the main event
  // processing.  This is typically very rare, particularly in user
  // code.  It is mainly used in implementing the NTupleSvc.
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode MyxAODAnalysis :: finalize ()
{
  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk.  This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged.  This is different from histFinalize() in that it only
  // gets called on worker nodes that processed input events.
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode MyxAODAnalysis :: histFinalize ()
{
  // This method is the mirror image of histInitialize(), meaning it
  // gets called after the last event has been processed on the worker
  // node and allows you to finish up any objects you created in
  // histInitialize() before they are written to disk.  This is
  // actually fairly rare, since this happens separately for each
  // worker node.  Most of the time you want to do your
  // post-processing on the submission node after all your histogram
  // outputs have been merged.  This is different from finalize() in
  // that it gets called on all worker nodes regardless of whether
  // they processed input events.
  return EL::StatusCode::SUCCESS;
}